/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include <i2c.h>
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "BMP280.h"
#include "lcd.h"
#include "ringbuffer_dma.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* Data structure for BMP280 output */
typedef struct {
    double temperature, pressure, alt;
} SensorData;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define QNH 1032
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/*
bluetooth part
*/

/* Ringbuffer for Rx messages */
RingBuffer_DMA rx_buf;
/* Array for DMA to save Rx bytes */
#define BUF_SIZE 256
uint8_t rx[BUF_SIZE];
uint32_t rx_count = 0;
/* Array for received commands */
char cmd[128];
uint8_t icmd = 0;
/* Array for Tx messages */
uint8_t tx[100];
/* Array for DMA to save ADC values */
uint16_t adc_buf[2];


/* USER CODE END Variables */
osThreadId ReadSensorTaskHandle;
osThreadId DisplayTaskHandle;
osThreadId CommunicationTaHandle;

osMailQId mailSensorsDataHandle;

osMutexId writeMutexHandle;
osMutexId commMutexHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void StartReadSensor(void const * argument);
void StartDisplay(void const * argument);
void StartCommunication(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Create the mutex(es) */
  /* definition and creation of writeMutex */
  osMutexDef(writeMutex);
  writeMutexHandle = osMutexCreate(osMutex(writeMutex));

  /* definition and creation of commMutex */
  osMutexDef(commMutex);
  commMutexHandle = osMutexCreate(osMutex(commMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of ReadSensorTask */
  osThreadDef(ReadSensorTask, StartReadSensor, osPriorityHigh, 0, 256);
  ReadSensorTaskHandle = osThreadCreate(osThread(ReadSensorTask), NULL);

  /* definition and creation of DisplayTask */
  osThreadDef(DisplayTask, StartDisplay, osPriorityLow, 0, 256);
  DisplayTaskHandle = osThreadCreate(osThread(DisplayTask), NULL);

  /* definition and creation of CommunicationTa */
  osThreadDef(CommunicationTa, StartCommunication, osPriorityNormal, 0, 256);
  CommunicationTaHandle = osThreadCreate(osThread(CommunicationTa), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of mailSensorsData */
/* what about the sizeof here??? cd native code */

//  osMessageQDef(mailSensorsData, 16, SensorData);
//  mailSensorsDataHandle = osMessageCreate(osMessageQ(mailSensorsData), NULL);

    /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */

    osMailQDef(mailSensorsData, 16, SensorData);
    mailSensorsDataHandle = osMailCreate(osMailQ(mailSensorsData), NULL);

    /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_StartReadSensor */
/**
  * @brief  Function implementing the ReadSensorTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartReadSensor */
void StartReadSensor(void const * argument)
{

  /* USER CODE BEGIN StartReadSensor */
    /* Mail queue variables */
    SensorData* data;

    osStatus status;

    /* BPM280 variables */
    bmp280_t bmp280;
    bmp280.i2c_handle = &hi2c1;
    bmp280.dev_addr = BMP280_I2C_ADDRESS1;

    int8_t com_rslt;

    com_rslt = BMP280_init(&bmp280);
    com_rslt += BMP280_set_power_mode(BMP280_NORMAL_MODE);
    com_rslt += BMP280_set_work_mode(BMP280_STANDARD_RESOLUTION_MODE);
    com_rslt += BMP280_set_standby_durn(BMP280_STANDBY_TIME_1_MS);

    /* Infinite loop */
    for(;;)
  {

          data = (SensorData *) osMailAlloc(mailSensorsDataHandle, 5);
          if (data != NULL) {

              BMP280_read_temperature_double(&data->temperature);
              BMP280_read_pressure_double(&data->pressure);
              data->alt = BMP280_calculate_altitude(QNH * 100);
//              data->alt = 0;

              /* Put the mail in the queue */
              osMailPut(mailSensorsDataHandle, data);

//      Sensors таска отправляет считанные данные в Mail Queue и, помимо этого, записывает их в структуру, которая доступна глобально. Запись в структуру обернута mutex'ом.

/*      if (status == osOK) {

      }

      status = osMutexWait(writeMutexHandle, osWaitForever);*/
          }
      osDelay(10);
  }
  /* USER CODE END StartReadSensor */
}

/* USER CODE BEGIN Header_StartDisplay */
/**
* @brief Function implementing the DisplayTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDisplay */
void StartDisplay(void const * argument)
{
  /* USER CODE BEGIN StartDisplay */

    SensorData* data;
    osStatus status;
    osEvent evt;

    uint32_t rx = 0;

    LCD_Init();
    /* Infinite loop */
  for(;;)
  {
      /* Get new mail from the queue */
      evt = osMailGet(mailSensorsDataHandle, osWaitForever);

      if (evt.status == osEventMail) {

          /* Obtain data from mail */
        data = (SensorData *) evt.value.p;

        /* print out */
          LCD_SetCursor(0, 24);
//          LCD_FillScreen(BLACK);
          LCD_Printf("%d\n", rx++);
          LCD_Printf("Temp : %6.2f C\n", data->temperature);
          LCD_Printf("Press: %6.0f Pa\n", data->pressure);
          LCD_Printf("Alt  : %3.0f m", data->alt);

          /* Free the cell in mail queue */
        osMailFree(mailSensorsDataHandle, data);

      }
      osDelay(40);
  }
  /* USER CODE END StartDisplay */
}

/* USER CODE BEGIN Header_StartCommunication */
/**
* @brief Function implementing the CommunicationTa thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartCommunication */
void StartCommunication(void const * argument)
{
  /* USER CODE BEGIN StartCommunication */
  /* Infinite loop */
  for(;;)
  {
    osDelay(20);
  }
  /* USER CODE END StartCommunication */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
